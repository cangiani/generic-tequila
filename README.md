# Tequila proxy
This project is an attempt to provide a simple way of _tequilizing_ a web
service. Tequila is the SSO service of [EPFL](https://www.epfl.ch/).

The idea is that, if the web service does not implement any authentication 
or access control, we can provide some sort of minimal access control via a
proxy.

There are two alternatives.

 1. using apache as proxy and `mod_tequila` for authentication (working example in `with_apache_mod_tequila` directory);
 2. using traefik as proxy and the openidconnect authentication feature with an
interposed [satosa](https://github.com/IdentityPython/SATOSA) instance for 
translating between traefik's openidconnect and tequila's saml2 (under development in `with_traefik_openid`).


Both examples require a running traefik on network `traefik` as explained in 
[here](https://github.com/multiscan/dev_traefik). Feel free to edit the 
`docker-compose.yml` files if you prefer a different setup.